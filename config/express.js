const express = require('express');
const config = require('config');
const consign = require('consign');
const pgp = require('pg-promise')({})
module.exports = () => {
    const app = express();
    cn = {
        'host': 'devweb.chiqahlpkytu.sa-east-1.rds.amazonaws.com',
        'port': 5432,
        'database': 'postgres',
        'user': 'root',
        'password': '123456789'
    };
    const db = pgp(cn);
    app.set('port', process.env.PORT || config.get('server.port'));
    app.db = db;
    app.use(function(req, res, next) {
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
        res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
        res.setHeader('Access-Control-Allow-Credentials', true);
        next();
    });
    app.use(express.urlencoded({extended:false}));
    app.use(express.json());
    consign({cwd: 'api'})
        .then('data')
        .then('controllers')
        .then('routes')
        .into(app);
    return app;
}