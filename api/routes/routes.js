module.exports = app => {

    const grupo = app.controllers.GrupoController;
    const projeto = app.controllers.ProjetoController;
    const usuario = app.controllers.UsuarioController;
    const tarefaStatus = app.controllers.TarefaStatusController;
    const tarefaTipo = app.controllers.TarefaTipoController;
    const tarefa = app.controllers.TarefaController;
    const projetoMembro = app.controllers.ProjetoMembroController;
    const prioridade = app.controllers.PrioridadeController;
    const comentario = app.controllers.ComentarioController;
    const login = app.controllers.LoginController;
    const sistema = app.controllers.SistemaController;

    app.route('/login')
        .post(
            login.login
        )

    app.route('/sistema')
        .post(
            sistema.criaSistema
        )
        .get(
            sistema.getAllSistemas
        );

    app.route('/sistema/:id')
        .put(
            sistema.putSistema
        )
        .delete(
            sistema.deleteSistema
        );

    app.route('/grupo')
        .post(
            grupo.criaGrupo
        )
        .get(
            grupo.getAllGrupo
        )
    app.route('/grupo/:id')
        .put(
           grupo.putGrupo
        )
        .delete(
            grupo.deleteGrupo
        )

    app.route('/comentario')
        .post(
            comentario.criaComentario
        )
        .get(
            comentario.getComentariosByTarefa
        )
    app.route('/comentario/:id')
        .post(
            comentario.putComentario
        )
        .delete(
            comentario.deleteComentario
        )


    app.route('/prioridade')
        .post(
            prioridade.criaPrioridade
        )
        .get(
            prioridade.getAllPrioridade
        );
    app.route('/prioridade/:id')
        .put(
            prioridade.putPrioridade
        )
        .delete(
            prioridade.deletePrioridade
        );

    app.route('/projeto')
        .post(
            projeto.criaProjeto
        )
        .get(
            projeto.getAllProjetos
        );
    app.route('/projeto/:id')
        .put(
            projeto.putProjeto
        )
        .delete(
            projeto.deleteProjeto
        );

    app.route('/projeto-membro')
        .post(
            projetoMembro.criaProjetoMembro
        )
        .get(
            projetoMembro.getAllProjetoMembros
        );
    app.route('/projeto-membro/:id')
        .delete(
            projetoMembro.deleteProjetoMembro
        );

    app.route('/tarefa')
        .post(
            tarefa.criaTarefa
        )
        .get(
            tarefa.getAllTarefa
        )
        .put(
            tarefa.putTarefa
        )
    app.route('/tarefa/:id')
        .put(
            tarefa.putTarefa
        )
        .delete(
            tarefa.deleteTarefa
        );

    app.route('/tarefa-list')
    .get(
        tarefa.getTarefaList
    )
    
    app.route('/tarefa-status')
        .post(
            tarefaStatus.criaTarefaStatus
        )
        .get(
            tarefaStatus.getAllTarefaStatus
        )
        .put(
            tarefaStatus.putTarefaStatus
        );

    app.route('/tarefa-status/:id')
        .put(
            tarefaStatus.putTarefaStatus
        )
        .delete(
            tarefaStatus.deleteTarefaStatus
        );

    app.route('/tarefa-tipo')
        .post(
            tarefaTipo.criaTarefaTipo
        )
        .get(
            tarefaTipo.getAllTarefaTipo
        );

    app.route('/tarefa-tipo/:id')
        .put(
            tarefaTipo.putTarefaTipo
        )
        .delete(
            tarefaTipo.deleteTarefaTipo
        );

    app.route('/usuario')
        .post(
            usuario.criaUsuario
        )
        .get(
            usuario.getAllUsuarios
        )
    app.route('/usuario/:id')
        .put(
            usuario.putUsuario
        )
        .delete(
            usuario.deleteUsuario
        )
}
