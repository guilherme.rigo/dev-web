module.exports = app => {

    const controller = {};

    controller.criaTarefa = function(req, res, next) {
        
        const titulo = req.body.titulo
        const descricao = req.body.descricao
        const id_projeto = req.body.id_projeto
        const id_criador = req.body.id_criador
        const id_dev = req.body.id_dev
        const tempo_estimado = req.body.tempo_estimado
        const data_inicio = req.body.data_inicio
        const data_fim = req.body.data_fim
        const id_tarefa_pai = req.body.id_pai_tarefa
        const id_tipo_tarefa = req.body.id_tipo_tarefa
        const created_at = new Date().toUTCString()
        const tempo_realizado = req.body.tempo_realizado
        const id_tarefa_status = req.body.id_status_tarefa
        const id_prioridade = req.body.id_prioridade
        const impacto = req.body.impacto
        const tarefa = req.body
        app.db.any(`INSERT INTO public.tarefa(descricao, titulo, data_inicio, data_fim, tempo_estimado, 
            id_pai_tarefa, id_tipo_tarefa, id_status_tarefa, id_projeto, id_criador, id_dev,
            id_prioridade, impacto, created_at, tempo_realizado) VALUES ('${descricao}', '${titulo}', '${data_inicio}', '${data_fim}', ${tempo_estimado}, 
            ${id_tarefa_pai}, ${id_tipo_tarefa}, ${id_tarefa_status}, ${id_projeto}, ${id_criador}, ${id_dev},
            ${id_prioridade}, ${impacto}, '${created_at}', ${tempo_realizado});`)
        .then(
            data => {
                res.status(201)
                    .json({
                        status: 'success',
                        data: tarefa,
                        message: 'Tarefa criada!'
                    })
                    
            }).catch(function (err){
                return next(err);
            });
    }

    controller.getAllTarefa = function (req, res, next) {
        if (req.query.id) {
            controller.getTarefaById(req, res, next)
        } else {
            app.db.any(`SELECT t.* FROM tarefa t`)
                .then(data => {
                    res.status(200)
                        .json({
                            status: 'success',
                            data: data,
                            message: 'Todas as tarefas'
                        });
                })
                .catch(function (err) {
                    return next(err);
                });
        }
    }

    controller.getTarefaList = function (req, res, next) {
        if (req.query.id) {
            controller.getTarefaById(req, res, next)
        } else {
            app.db.any(`SELECT t.id, t.titulo, to_char(t.data_inicio, 'DD/MM/YYYY') AS data_inicio,
            to_char(t.data_fim, 'DD/MM/YYYY') as data_fim, u.nome as id_criador, dev.nome as dev, p.titulo as projeto, tp.descricao as tipo,
            s.descricao as status, pr.descricao as prioridade
            FROM tarefa t
            join usuario u on u.id_usuario = t.id_criador
            join projeto p on p.id = t.id_projeto
            join tarefa_tipo tp on t.id_tipo_tarefa = tp.id
            join tarefa_status s on s.id = t.id_status_tarefa
            join usuario dev on dev.id_usuario = t.id_dev
            join prioridade pr on pr.id = t.id_prioridade
            `)
                .then(data => {
                    res.status(200)
                        .json({
                            status: 'success',
                            data: data,
                            message: 'Todas as tarefas'
                        });
                })
                .catch(function (err) {
                    return next(err);
                });
        }
    }
    

    controller.getTarefaById = function (req, res, next) {
        var id = parseInt(req.query.id);
        console.log(id)
        app.db.any(`SELECT t.*, u.nome as id_criador, dev.nome as dev, ts.descricao as status FROM tarefa t 
        join usuario u on t.id_criador = u.id_usuario
        join usuario dev on dev.id_usuario = t.id_dev
        join tarefa_status ts on ts.id = t.id_status_tarefa 
        WHERE t.id = $1`, id)
            .then(data => {
                res.status(200)
                    .json({
                        status: 'success',
                        data: data
                    });
            })
            .catch(function (err) {
                return next(err);
            });
    }


    controller.deleteTarefa = function (req, res, next) {
        const { id } = req.params;
        app.db.any(`DELETE FROM tarefa where id = $1`, id)
            .then(data => {
                res.status(200)
                    .json({
                        status: 'success'
                    });
            })
            .catch(function (err) {
                return next(err);
            });
    }

    controller.putTarefa = function(req, res, next) {
        console.log(req.query)
        const { id } = req.query;
        console.log(id)
        const { titulo, descricao, id_projeto, id_criador, id_dev, tempo_estimado, 
            data_inicio, data_fim, id_pai_tarefa, id_tipo_tarefa, id_status_tarefa, 
            data_inicio_dev, data_fim_dev, tempo_realizado, 
            id_prioridade, complexidade, impacto, id_grupo} = req.body

            try {
                if(titulo != undefined && titulo != null)
                    app.db.any(`UPDATE tarefa set titulo = '${titulo}' where id = $1`, id)
                if(descricao != undefined && descricao != null)
                    app.db.any(`UPDATE tarefa set descricao = '${req.body.descricao}' where id = $1`, id)
                if(id_projeto != undefined && id_projeto != null)
                    app.db.any(`UPDATE tarefa set id_projeto = '${req.body.id_projeto}' where id = $1`, id)
                if(id_criador != undefined && id_criador != null)
                    app.db.any(`UPDATE tarefa set id_criador = '${req.body.id_criador}' where id = $1`, id)
                if(id_dev != undefined && id_dev != null)
                    app.db.any(`UPDATE tarefa set id_dev = '${req.body.id_dev}' where id = $1`, id)
                if(tempo_estimado != undefined && tempo_estimado != null)
                    app.db.any(`UPDATE tarefa set tempo_estimado = '${req.body.tempo_estimado}' where id = $1`, id)
                if(data_inicio != undefined && data_inicio != null)
                    app.db.any(`UPDATE tarefa set data_inicio = '${req.body.data_inicio}' where id = $1`, id)
                if(data_fim != undefined && data_fim != null)
                    app.db.any(`UPDATE tarefa set data_fim = '${req.body.data_fim}' where id = $1`, id)
                if(id_pai_tarefa != undefined && id_pai_tarefa != null)
                    app.db.any(`UPDATE tarefa set id_pai_tarefa = '${req.body.id_pai_tarefa}' where id = $1`, id)
                if(id_tipo_tarefa != undefined && id_tipo_tarefa != null)
                    app.db.any(`UPDATE tarefa set id_tipo_tarefa = '${req.body.id_tipo_tarefa}' where id = $1`, id)
                if(id_status_tarefa != undefined && id_status_tarefa != null)
                    app.db.any(`UPDATE tarefa set id_status_tarefa = '${req.body.id_status_tarefa}' where id = $1`, id)
                if(data_inicio_dev != undefined && data_inicio_dev != null)
                    app.db.any(`UPDATE tarefa set data_inicio_dev = '${req.body.data_inicio_dev}' where id = $1`, id)
                if(data_fim_dev != undefined && data_fim_dev != null)
                    app.db.any(`UPDATE tarefa set data_fim_dev = '${req.body.data_fim_dev}' where id = $1`, id)
                if(tempo_realizado != undefined && tempo_estimado != null)
                    app.db.any(`UPDATE tarefa set tempo_estimado = '${req.body.tempo_estimado}' where id = $1`, id)
                if(id_prioridade != undefined && id_prioridade != null)
                    app.db.any(`UPDATE tarefa set id_prioridade = '${req.body.id_prioridade}' where id = $1`, id)
                if(complexidade != undefined && complexidade != null)
                    app.db.any(`UPDATE tarefa set complexidade = '${req.body.complexidade}' where id = $1`, id)
                if(impacto != undefined && impacto != null)
                    app.db.any(`UPDATE tarefa set impacto = '${req.body.impacto}' where id = $1`, id)
                if(id_grupo != undefined && id_grupo != null)
                    app.db.any(`UPDATE tarefa set id_grupo = '${req.body.id_grupo}' where id = $1`, id)   
                    
                const updated_at = new Date().toUTCString()
                app.db.any(`UPDATE tarefa set updated_at = '${updated_at}' where id = $1`, id)   
                
                return res.status(200).json({status: 'success'});
            } catch (err) {
                return next(err);
            }     
        }
    
    return controller;
}