module.exports = app => {

    const controller = {};

    controller.criaUsuario = async function(req, res, next) {
        
        const nome = req.body.email
        const senha = req.body.senha
        const id_sistema = req.body.id_sistema

        const usuario = req.body

        await app.db.any(`INSERT INTO usuario (nome) 
        VALUES ('${nome}');`)
        .then(
            data => {
                res.status(201)
                    .json({
                        status: 'success',
                        data: usuario,
                        message: 'Usuário criado!'
                    })
                    .catch(function (err){
                        return next(err);
                    })
            });

        res.status(201).json(usuario);
    }

    controller.getAllUsuarios = function(req, res, next) {
        
        if(req.query.id){
            controller.getUsuarioById(req, res, next)
        } else {
            app.db.any(`SELECT id_usuario as id, * FROM usuario`)
                .then(data => {
                    res.status(200)
                        .json({
                            status: 'success',
                            data: data,
                            message: 'Todos os Usuarios'
                        });
                })
            .catch(function (err){
                return next(err);
            });
        }
    }

    controller.getUsuarioById = function(req, res, next){
        var id = parseInt(req.query.id);

        app.db.any(`SELECT * FROM usuario WHERE id_usuario = $1`, id)
            .then(data => {
                res.status(200)
                    .json({
                        status: 'success',
                        data: data                        
                    });
            })
        .catch(function (err){
            return next(err);
        });
    }

    controller.putUsuario = function(req, res, next) {
        const { id } = req.params;
        app.db.any(`UPDATE usuario set nome = '${req.body.nome}' where id_usuario = $1`, id)
        .then(data => {
            res.status(200)
                .json({
                    status: 'success'                   
                });
        })
        .catch(function (err){
            return next(err);
        });
    }

    controller.deleteUsuario = function(req, res, next) {
        const { id } = req.params;
        console.log(id)
        app.db.any(`DELETE FROM projeto_usuario where id_usuario = $1`, id)
        app.db.any(`DELETE FROM usuario where id_usuario = $1`, id)
        .then(data => {
            res.status(200)
                .json({
                    status: 'success'                      
                });
        })
        .catch(function (err){
            return next(err);
        });
    }
    
    return controller;
}
