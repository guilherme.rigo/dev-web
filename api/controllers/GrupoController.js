module.exports = app => {

    const controller = {};

    controller.criaGrupo = function (req, res) {

        const grupo = req.body.grupo

        app.db.none(`INSERT INTO grupo (descricao) 
        VALUES ('${req.body.descricao}')`)
            .then(
                data => {
                    res.status(200)
                        .json({
                            status: 'success',
                            data: data,
                            message: 'Grupo criado!'
                        })
                        .catch(function (err) {
                            return next(err);
                        })
                });

        res.status(200).json(grupo);
    }

    controller.getAllGrupo = function (req, res, next) {

        if (req.query.id) {
            controller.getGrupoById(req, res, next)
        } else {
            app.db.any('SELECT * FROM grupo')
                .then(data => {
                    res.status(200)
                        .json({
                            status: 'success',
                            data: data,
                            message: 'Todos os grupos'
                        });
                })
                .catch(function (err) {
                    return next(err);
                });
        }
    }

    controller.getGrupoById = function (req, res, next) {
        var id = parseInt(req.query.id);

        app.db.any('SELECT * FROM grupo WHERE id = $1', id)
            .then(data => {
                res.status(200)
                    .json({
                        status: 'success',
                        data: data
                    });
            })
            .catch(function (err) {
                return next(err);
            });
    }

       controller.putGrupo = function (req, res, next) {
           console.log(req, res, next);
           const { id, id_projeto, descricao } = req.body;
           if(id_projeto != undefined && id_projeto != null)
                app.db.any(`UPDATE grupo set id_projeto = '${id_projeto}' where id = $1`, id)
                    .then(data => {
                        res.status(200)
                            .json({
                                status: 'success',
                                data: data
                            });
                    })
                    .catch(function (err) {
                    return next(err);
                    });
           if (descricao != undefined && descricao != null)
               app.db.any(`UPDATE grupo set descricao = '${descricao}' where id = $1`, id)
                   .then(data => {
                       res.status(200)
                           .json({
                               status: 'success',
                               data: data
                           });
                   })
                   .catch(function (err) {
                      return next(err);
                   });

            return 'Grupo não modificado!';
       }

    controller.deleteGrupo = function (req, res, next) {
        const { id } = req.params;
        app.db.any(`DELETE FROM grupo where id = $1`, id)
            .then(data => {
                res.status(200)
                    .json({
                        status: 'success'
                    });
            })
            .catch(function (err) {
                return next(err);
            });
    }

    return controller;
}