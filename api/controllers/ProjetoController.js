module.exports = app => {

    const controller = {};

    controller.criaProjeto = async function (req, res, next) {

        const titulo = req.body.titulo
        const descricao = req.body.descricao
        const data_criacao = new Date().toUTCString()
        const data_inicio = req.body.data_inicio
        const data_fim = req.body.data_fim
        const id_criador = req.body.id_criador
        const id_sistema = req.body.id_sistema

        await app.db.any(`INSERT INTO projeto (titulo, descricao, created_at, data_inicio, data_fim, id_sistema, id_criador)
            VALUES ('${titulo}', '${descricao}', '${data_criacao}', '${data_inicio}', '${data_fim}', '${id_sistema}', '${id_criador}')`)
            .then(
              async () => {
                   await getUltimoProjetoCriado(res, next)
                }).catch(function (err) {
                    return next(err);
                });
    }

    const getUltimoProjetoCriado = async (res, next) => {
        await app.db.any(`SELECT * from projeto order by id desc`).then(data => {
            res.status(201)
            .json({
                status: 'success',
                data: data[0],
                message: 'Projeto Criado!'
            })
        }).catch(function (err) {
            return next(err);
        });
    }

    controller.getAllProjetos = function (req, res, next) {

        if (req.query.id) {
            controller.getProjetoById(req, res, next)
        } else {
            app.db.any(`SELECT *, to_char(data_inicio, 'DD/MM/YYYY') as inicio, to_char(data_fim, 'DD/MM/YYYY') as fim, 
                u.nome as criador
                from projeto
                join usuario u on u.id_usuario = id_criador`)
                .then(data => {
                    res.status(200)
                        .json({
                            status: 'success',
                            data: data,
                            message: 'Todos os projetos'
                        });
                })
                .catch(function (err) {
                    return next(err);
                });
        }
    }

    controller.getProjetoById = function (req, res, next) {
        var id = parseInt(req.query.id);

        app.db.any(`SELECT * FROM projeto WHERE id = $1`, id)
            .then(data => {
                res.status(200)
                    .json({
                        status: 'success',
                        data: data
                    });
            })
            .catch(function (err) {
                return next(err);
            });
    }

    controller.putProjeto = function (req, res, next) {
        const { id } = req.params;
        const { titulo, descricao, data_inicio, data_fim, id_criador, id_sistema } = req.body;

        if (titulo != undefined && titulo != null)
            app.db.any(`UPDATE projeto set titulo = '${titulo}' where id = $1`, id)
        if (descricao != undefined && descricao != null)
            app.db.any(`UPDATE projeto set descricao = '${descricao}' where id = $1`, id)
        if (data_inicio != undefined && data_inicio != null)
            app.db.any(`UPDATE projeto set data_inicio = '${req.body.data_inicio}' where id = $1`, id)
        if (data_fim != undefined && data_fim != null)
            app.db.any(`UPDATE projeto set data_fim = '${req.body.data_fim}' where id = $1`, id)
        if (id_criador != undefined && id_criador != null)
            app.db.any(`UPDATE projeto set id_criador = '${id_criador}' where id = $1`, id)
                //if (id_sistema != undefined && id_sistema != null)
                // app.db.any(`UPDATE projeto set id_sistema = '${id_sistema}' where id = $1`, id)

                .then(data => {
                    res.status(200)
                        .json({
                            status: 'success'
                        });
                })
                .catch(function (err) {
                    return next(err);
                });
    }

    controller.deleteProjeto = function(req, res, next) {
        const { id } = req.params;
        app.db.any(`DELETE FROM projeto_usuario where id_projeto = $1`, id)
        app.db.any(`DELETE FROM projeto where id = $1`, id)
            .then(data => {
                res.status(200)
                    .json({
                        status: 'success'
                    });
            })
            .catch(function (err) {
                return next(err);
            });
    }


    return controller;
}