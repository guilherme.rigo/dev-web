module.exports = app => {

    const controller = {};

    controller.criaPrioridade = function(req, res) {

        const prioridade = req.body.prioridade

        app.db.none(`INSERT INTO prioridade (descricao) 
        VALUES ('${req.body.descricao}')`)
        .then(
            data => {
                res.status(200)
                    .json({
                        status: 'success',
                        data: data,
                        message: 'Prioridade criada!'
                    })
                    .catch(function (err){
                        return next(err);
                    })
            });

        res.status(200).json(prioridade);
    }

    controller.getAllPrioridade = function(req, res, next) {
        
        if(req.query.id){
            controller.getPrioridadeById(req, res, next)
        } else {
            app.db.any('SELECT * FROM prioridade')
                .then(data => {
                    res.status(200)
                        .json({
                            status: 'success',
                            data: data,
                            message: 'Todas as prioridades'
                        });
                })
            .catch(function (err){
                return next(err);
            });
        }
    }

    controller.getPrioridadeById = function(req, res, next){
        var id = parseInt(req.query.id);

        app.db.any('SELECT * FROM prioridade WHERE id = $1', id)
            .then(data => {
                res.status(200)
                    .json({
                        status: 'success',
                        data: data                        
                    });
            })
        .catch(function (err){
            return next(err);
        });
    }

    controller.putPrioridade = function(req, res, next) {
        const { id } = req.params;
        app.db.any(`UPDATE prioridade set descricao = '${req.body.descricao}' where id = $1`, id)
        .then(data => {
            res.status(200)
                .json({
                    status: 'success'                   
                });
        })
        .catch(function (err){
            return next(err);
        });
    }

    controller.deletePrioridade = function(req, res, next) {
        const { id } = req.params;
        app.db.any(`DELETE FROM prioridade where id = $1`, id)
        .then(data => {
            res.status(200)
                .json({
                    status: 'success'                      
                });
        })
        .catch(function (err){
            return next(err);
        });
    }
    
    return controller;
}