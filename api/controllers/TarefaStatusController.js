module.exports = app => {

    const controller = {};

    controller.criaTarefaStatus = function(req, res, next) {

        const tarefaStatus = req.body.tarefaStatus

        app.db.none(`INSERT INTO tarefa_status (descricao) 
        VALUES ('${req.body.descricao}')`)
        .then(
            data => {
                res.status(200)
                    .json({
                        status: 'success',
                        data: data,
                        message: 'Tarefa status criado!'
                    })
                    .catch(function (err){
                        return next(err);
                    })
            });

        res.status(200).json(tarefaStatus);
    }

    controller.getAllTarefaStatus = function(req, res, next) {
        
        if(req.query.id){
            controller.getTarefaStatusById(req, res, next)
        } else {
            app.db.any('SELECT * FROM tarefa_status')
                .then(data => {
                    res.status(200)
                        .json({
                            status: 'success',
                            data: data,
                            message: 'Todos os status de tarefa'
                        });
                })
            .catch(function (err){
                return next(err);
            });
        }
    }

    controller.getTarefaStatusById = function(req, res, next){
        var id = parseInt(req.query.id);

        app.db.any('SELECT * FROM tarefa_status WHERE id = $1', id)
            .then(data => {
                res.status(200)
                    .json({
                        status: 'success',
                        data: data                        
                    });
            })
        .catch(function (err){
            return next(err);
        });
    }

    controller.putTarefaStatus = function(req, res, next) {
        const id = parseInt(req.query.id);
        app.db.any(`UPDATE tarefa_status set descricao = '${req.body.descricao}' where id = $1`, id)
        .then(data => {
            res.status(200)
                .json({
                    status: 'success'                   
                });
        })
        .catch(function (err){
            return next(err);
        });
    }

    controller.deleteTarefaStatus = function(req, res, next) {
        const { id } = req.params;
        app.db.any(`DELETE FROM tarefa_status where id = $1`, id)
        .then(data => {
            res.status(200)
                .json({
                    status: 'success'                      
                });
        })
        .catch(function (err){
            return next(err);
        });
    }
    
    return controller;
}