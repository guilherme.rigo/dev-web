module.exports = app => {

    const controller = {};

    controller.criaProjetoMembro = function(req, res, next) {
        
        const id_projeto = req.body.id_projeto
        const idUsuarios = req.body.id_usuarios
        try {
            idUsuarios.forEach(usuarios => {
                console.log(usuarios.id_usuario)
                app.db.none(`INSERT INTO projeto_usuario (id_projeto, id_usuario) 
                    VALUES ('${id_projeto}', '${usuarios.id_usuario}')`)
            });
            res.status(200)
            .json({
                status: 'success',
                message: 'Usuário(s) vinculado(s) ao projeto!'
            })
        } catch(err) {
            return next(err);
        }
    }

    controller.getAllProjetoMembros = function(req, res, next) {
        
        if(req.query.id){
            controller.getProjetoMembroById(req, res, next)
        } else {
            app.db.any(`SELECT p.titulo as projeto, u.nome as usuario FROM projeto_usuario pm 
                        join projeto p on p.id = pm.id_projeto 
                        join usuario u on pm.id_usuario = u.id_usuario`)
                .then(data => {
                    res.status(200)
                        .json({
                            status: 'success',
                            data: data,
                            message: 'Todos os Membros de projetos'
                        });
                })
            .catch(function (err){
                return next(err);
            });
        }
    }

    controller.getProjetoMembroById = function(req, res, next){
        var id = parseInt(req.query.id);

        app.db.any(`SELECT p.titulo as projeto, u.nome as usuario FROM projeto_usuario pm 
                    join projeto p on p.id = pm.id_projeto 
                    join usuario u on pm.id_usuario = u.id_usuario WHERE pm.id = $1`, id)
            .then(data => {
                res.status(200)
                    .json({
                        status: 'success',
                        data: data                        
                    });
            })
        .catch(function (err){
            return next(err);
        });
    }

    controller.deleteProjetoMembro = function (req, res, next) {
        const { id } = req.params;
        app.db.any(`DELETE FROM projeto_usuario where id = $1`, id)
            .then(data => {
                res.status(200)
                    .json({
                        status: 'success'
                    });
            })
            .catch(function (err) {
                return next(err);
            });
    }
    
    return controller;
}