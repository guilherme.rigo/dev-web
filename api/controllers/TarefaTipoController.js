module.exports = app => {

    const controller = {};

    controller.criaTarefaTipo = function (req, res) {

        const tarefaTipo = req.body.tarefaTipo

        app.db.none(`INSERT INTO tarefa_tipo (descricao) 
        VALUES ('${req.body.descricao}')`)
            .then(
                data => {
                    res.status(200)
                        .json({
                            status: 'success',
                            data: data,
                            message: 'Tarefa Tipo criado!'
                        })
                        .catch(function (err) {
                            return next(err);
                        })
                });

        res.status(200).json(tarefaTipo);
    }

    controller.getAllTarefaTipo = function (req, res, next) {

        if (req.query.id) {
            controller.getTarefaTipoById(req, res, next)
        } else {
            app.db.any('SELECT * FROM tarefa_tipo')
                .then(data => {
                    res.status(200)
                        .json({
                            status: 'success',
                            data: data,
                            message: 'Todos os tipos de tarefa'
                        });
                })
                .catch(function (err) {
                    return next(err);
                });
        }
    }

    controller.getTarefaTipoById = function (req, res, next) {
        var id = parseInt(req.query.id);

        app.db.any('SELECT * FROM tarefa_tipo WHERE id = $1', id)
            .then(data => {
                res.status(200)
                    .json({
                        status: 'success',
                        data: data
                    });
            })
            .catch(function (err) {
                return next(err);
            });
    }

    controller.putTarefaTipo = function (req, res, next) {
        const { id } = req.params;
        app.db.any(`UPDATE tarefa_tipo set descricao = '${req.body.descricao}' where id = $1`, id)
            .then(data => {
                res.status(200)
                    .json({
                        status: 'success'
                    });
            })
            .catch(function (err) {
                return next(err);
            });
    }

    controller.deleteTarefaTipo = function (req, res, next) {
        const { id } = req.params;
        app.db.any(`DELETE FROM tarefa_tipo where id = $1`, id)
            .then(data => {
                res.status(200)
                    .json({
                        status: 'success'
                    });
            })
            .catch(function (err) {
                return next(err);
            });
    }

    return controller;
}