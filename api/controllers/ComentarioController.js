module.exports = app => {

    const controller = {};

    controller.criaComentario = function(req, res, next) {
        console.log(req.body)
        const id_usuario = req.body.id_usuario
        const id_tarefa = req.body.id_tarefa
        const created_at = new Date().toUTCString()
        const descricao = req.body.descricao
        const id_comentario_pai = req.body.id_comentario_pai

        app.db.none(`INSERT INTO comentario (id_usuario, id_tarefa, created_at, descricao) 
        VALUES (${id_usuario}, ${id_tarefa}, '${created_at}', '${descricao}');`)
        .then(async () => {
            await getUltimoComentarioCriado(res, next)
         }).catch(function (err) {
             return next(err);
         });

    }

    const getUltimoComentarioCriado = async (res, next) => {
        await app.db.any(`SELECT * from comentario order by id desc`).then(data => {
            res.status(201)
            .json({
                status: 'success',
                data: data[0],
                message: 'Comentario Criado!'
            })
        }).catch(function (err) {
            return next(err);
        });
    }

    controller.getComentariosByTarefa = function(req, res, next) {
        const {tarefa} = req.query
        console.log(req.query)
        if(req.query.id){
           return controller.getComentarioById(req, res, next)
        } else {
            app.db.any(`SELECT c.id, c.created_at as data, c.descricao, u.nome FROM comentario c
            join usuario u on c.id_usuario = u.id_usuario
            where c.id_tarefa = $1 order by data asc`, tarefa)
                .then(data => {
                    res.status(200)
                        .json({
                            status: 'success',
                            data: data,
                            message: 'Todos os Comentários'
                        });
                })
            .catch(function (err){
                return next(err);
            });
        }
    } 

    controller.getAllComentarios = function(req, res, next) {
        
        if(req.query.id){
           return controller.getComentarioById(req, res, next)
        } else {
            app.db.any('SELECT * FROM comentario')
                .then(data => {
                    res.status(200)
                        .json({
                            status: 'success',
                            data: data,
                            message: 'Todos os Comentários'
                        });
                })
            .catch(function (err){
                return next(err);
            });
        }
    }

    controller.getComentarioById = function(req, res, next){
        var id = parseInt(req.query.id);

        app.db.any('SELECT * FROM comentario WHERE id = $1', id)
            .then(data => {
                res.status(200)
                    .json({
                        status: 'success',
                        data: data                        
                    });
            })
        .catch(function (err){
            return next(err);
        });
    }

    controller.putComentario = function(req, res, next) {
        var id = parseInt(req.query.id);

        app.db.any(`UPDATE comentario SET descricao = ${req.body.descricao} WHERE id = $1`, id)
            .then(data => {
                res.status(200)
                    .json({
                        status: 'success',
                        data: data                        
                    });
            })
        .catch(function (err){
            return next(err);
        });

    }

    controller.deleteComentario = function(req, res, next) {
        var id = parseInt(req.params.id);

        app.db.any('DELETE FROM comentario WHERE id = $1', id)
            .then(data => {
                res.status(200)
                    .json({
                        status: 'success',
                        data: data                        
                    });
            })
        .catch(function (err){
            return next(err);
        });
    }
    
    return controller;
}