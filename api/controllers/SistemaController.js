module.exports = app => {

    const controller = {};

    controller.criaSistema = function (req, res) {
        app.db.none(`INSERT INTO sistema (nome) 
        VALUES ('${req.body.nome}')`)
            .then(
                data => {
                    res.status(200)
                        .json({
                            status: 'success',
                            data: data,
                            message: 'Sistema criado!'
                        })
                        .catch(function (err) {
                            return next(err);
                        })
                });
    }

    controller.getAllSistemas = function (req, res, next) {

        if (req.query.id) {
            controller.getSistemaById(req, res, next)
        } else {
            app.db.any('SELECT * FROM sistema')
                .then(data => {
                    res.status(200)
                        .json({
                            status: 'success',
                            data: data,
                            message: 'Todos os sistemas'
                        });
                })
                .catch(function (err) {
                    return next(err);
                });
        }
    }

    controller.getSistemaById = function (req, res, next) {
        var id = parseInt(req.query.id);

        app.db.any('SELECT * FROM sistema WHERE id_sistema = $1', id)
            .then(data => {
                res.status(200)
                    .json({
                        status: 'success',
                        data: data
                    });
            })
            .catch(function (err) {
                return next(err);
            });
    }

    controller.putSistema = function (req, res, next) {
        const { id } = req.params;
        app.db.any(`UPDATE sistema set nome = '${req.body.nome}' where id_sistema = $1`, id)
            .then(data => {
                res.status(200)
                    .json({
                        status: 'success'
                    });
            })
            .catch(function (err) {
                return next(err);
            });
    }

    controller.deleteSistema = function (req, res, next) {
        const { id } = req.params;
        app.db.any(`DELETE FROM sistema where id_sistema = $1`, id)
            .then(data => {
                res.status(200)
                    .json({
                        status: 'success'
                    });
            })
            .catch(function (err) {
                return next(err);
            });
    }

    return controller;
}